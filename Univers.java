import java.util.*;

public class Univers{
	int nb_jour;
	int [] list_book;
	LinkedList<Lib> list_lib;

	public Univers(int nb_jour, int [] list_book){
		this.nb_jour = nb_jour;
		this.list_book = list_book;
	}
	public void add_list_book(LinkedList<Lib> l){
		this.list_lib = l;
	}

	public void affiche(){
		System.out.println(list_book.length + " " + list_lib.size() + " " + nb_jour);
		for (int i = 0; i < list_book.length; i++) {
			System.out.print(list_book[i] + " ");
		}
		System.out.println();
		for (int j = 0; j < list_lib.size(); j++) {
			list_lib.get(j).affiche();
		}
	}
	public LinkedList get_list(){
		return list_lib;
	}

} 

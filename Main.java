import java.util.LinkedList;
import java.io.*;
import java.util.Collections;
import java.util.Comparator;

public class Main{

	public static void main(String[] args){
		Parser p = new Parser();
		int cpt = 1;
		String[] fics = {"problem_statement/a_example.txt", "problem_statement/b_read_on.txt", "problem_statement/c_incunabula.txt", "problem_statement/d_tough_choices.txt", "problem_statement/e_so_many_books.txt", "problem_statement/f_libraries_of_the_world.txt"};
		
		for(String s : fics) {
			Univers ne = p.parse(s);
			Collections.sort(ne.get_list(), new Comparator<Lib>() {
				@Override
				public int compare(Lib o1, Lib o2) {
					int res = o1.signup_time - o2.signup_time;
					if (res == 0)
						res = o2.books_per_day - o1.books_per_day;
					return res;
				}
			});
			try {
				FileWriter fw = new FileWriter("answer"+cpt+".txt");
				fw.write(ne.list_lib.size()+"\n");
				for(int i = 0; i < ne.list_lib.size(); i++) {
					fw.write(ne.list_lib.get(i).id+" "+ne.list_lib.get(i).nb_books);
					fw.write("\n");
					fw.write(ne.list_lib.get(i).tab_to_string()+"\n");
				}
				fw.close();
				cpt+=1;
			} catch(Exception e) {
				System.out.println("error writing answer: " + e);
			}
		}
	}
}
